/*
 GAME RULES:
 
 - The game has 2 players, playing in rounds  //array var socres declaration
 - In each turn, a player rolls a dice as many times as he 
 whishes. 
 Each result get added to his ROUND score
 //var round starts from 0 declaration
 - BUT, if the player rolls a 1, all his ROUND score gets lost. 
 After that, it's the next player's turn
 
 - The player can choose to 'Hold', 
 which means that his ROUND score gets 
 added to his GLBAL score. After that, it's the next 
 player's turn
 //if pa event listener 
 - The first player to reach 100 points on 
 GLOBAL score wins the game
 */
var scores, activePlayer, totalScore;
//selection
var scoreFirst = document.getElementById('score-0');
var scoreSecond = document.getElementById('score-1');
var currentFirst = document.getElementById('current-0');
var currentSecond = document.getElementById('current-1');
var holdButton = document.querySelector('.btn-hold');
//default values
function init() {
    scores = [0, 0];
    activePlayer = 0;
    totalScore = 0;

    //selektori polja
    scoreFirst.textContent = 0;
    scoreSecond.textContent = 0;
    currentFirst.textContent = 0;
    currentSecond.textContent = 0;
}
//inicijacija igre start
init();
//define number of players

//dice functionality
document.querySelector('.btn-roll').addEventListener('click', function () {
    dice = Math.floor(Math.random() * 6) + 1;
//    console.log(dice);
    //display result in picture of dice
    dicePNG = document.querySelector('.dice');
    dicePNG.style.display = 'block';
    dicePNG.src = 'dice-' + dice + '.png';
//current player in array
    for (activePlayer; activePlayer < scores.length; activePlayer++) {
        currentPlayer = activePlayer;
        break;
    }

    //if result 1 active is next player
    if (dice === 1) {
        nextPlayer();
        totalScore = 0;
        dice = 0;
        document.getElementById('current-' + activePlayer).textContent = dice;
        document.getElementById('score-' + activePlayer).textContent = totalScore;
        scores[activePlayer] = totalScore;
    } else {
        totalScore += dice;
        //ovo ispod je to isto samo drugi nacin
//        roundScore = roundScore + dice;
        document.querySelector('#current-' + activePlayer).textContent = totalScore;
        scores[activePlayer] = totalScore;


        //hold button
        holdButton.addEventListener('click', function () {
            scores[activePlayer] = totalScore;
            document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
            //if game over
            if (scores[activePlayer] >= 200) {
                document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
                document.querySelector('.dice').style.display = 'none';
                document.querySelector('.player-' + activePlayer + '-panel').classList.toggle('winner');
                document.querySelector('.player-' + activePlayer + '-panel').classList.toggle('active');
            } else {
                nextPlayer();
            }
        });
    }
    document.getElementById('current-' + currentPlayer).textContent = dice;
    //set total score value per player
    document.getElementById('score-' + currentPlayer).textContent = totalScore;
});
//next player function
function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
}

