/*
 GAME RULES:
 
 - The game has 2 players, playing in rounds 
 //array var socres declaration
 - In each turn, a player rolls a dice as many times as he 
 whishes. 
 Each result get added to his ROUND score
 //var round starts from 0 declaration
 - BUT, if the player rolls a 1, 
 all his ROUND score gets lost. 
 After that, it's the next player's turn
 
 //if statement
 
 - The player can choose to 'Hold', 
 which means that his ROUND score gets 
 added to his GLBAL score. After that, it's the next 
 player's turn
 //if pa event listener 
 - The first player to reach 100 points on 
 GLOBAL score wins the game
 */
//SECOND CODING TASK
/* 
 * your 3  challenges
 * Change the game to folow these rules:
 * 1.A player looses his Entire score when he rolls two 6 in a row.
 * After that , it is the next player`s turn. (Hint: Always save the previous 
 * dice roll in a separate variable)
 * 2.Add an input field to the HTML where players can set the winning score , 
 * so that they can change the predefined score of 100.
 * (HInt: you can read that value sixth the .value property in js
 * This is a good oportunity to use google to figure that out:)
 * 3.Add another dice to the game , so that there are two dices now.
 * The player looses his current score when one of them is a 1.
 * (HInt: you will need css to position the second 
 * dice so take a look at the css code for the first one)
 */
var scores, roundScore, activePlayer, gamePlaying, lastDice, dice1, dice2;
//state variable tells us condition of the system , kada hocemo da iskazemo stanje necega
//da li se igra igra ili ne true ili false gamePlaying

//start game
init();
//dodavanje event listenera za roll kocke funkcionalnost
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gamePlaying) {
        //Random number
        dice1 = Math.floor(Math.random() * 6) + 1;
        dice2 = Math.floor(Math.random() * 6) + 1;

        //Display the result
//        var diceDOM = document.querySelector('.dice');
//we need to select both dice insted of one , previous row
        document.getElementById('dice-1').style.display = 'block';
        document.getElementById('dice-2').style.display = 'block';

//        diceDOM.style.display = "block";
        document.getElementById('dice-1').style.display = "block";
        document.getElementById('dice-2').style.display = "block";
        document.getElementById('dice-1').src = 'dice-' + dice1 + '.png';
        document.getElementById('dice-2').src = 'dice-' + dice2 + '.png';
// update, if player get 6 on two dice he loses all score
        if (dice1 !== 1 && dice2 !== 1) {
            //update the round score if the rolled number was not a 1
//            roundScore += dice;
            roundScore += dice1 + dice2;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
        //task 2
        /*
         //player lose all score if win two 6 and also if win 1 lose and goes to next player too
         if (dice === 6 && lastDice === 6) {
         scores[activePlayer] = 0;
         document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
         nextPlayer();
         } else if (dice !== 1) {
         //update the round score if the rolled number was not a 1
         roundScore += dice;
         document.querySelector('#current-' + activePlayer).textContent = roundScore;
         } else {
         nextPlayer();
         }
         //var for saving previous dice roll
         lastDice = dice;
         */
    }
});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        scores[activePlayer] += roundScore;

        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
//2.update : promena vrednosti max rezultata za pobedu
        var input = document.querySelector(".final-score").value;
//        console.log(input);
//check if it is empty, undefined, 0 or null or ""
        var winningScore;
        if (input) {
            winningScore = input;
        } else {
            winningScore = 100;
        }
        if (scores[activePlayer] >= winningScore) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('#dice-1').style.display = 'none!';
            document.querySelector('#dice-2').style.display = 'none!';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        } else {
            nextPlayer();
        }
    }

});


function nextPlayer() {
    //predji na drugog igraca
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    //podesi sve vrednosti na nulu
    roundScore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    //podesi da je aktivan drugi igrac
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    //ne vidi se kocka dok se ne klikne na roll button
    document.querySelector('#dice-1').style.display = 'none';
    document.querySelector('#dice-2').style.display = 'none';
}
//ako se izabere nova igra pokrece se ponovo funkcija init
//klik na button new game
document.querySelector('.btn-new').addEventListener('click', init);
//init
function init() {
    //definisanje osnovnih globalnih varijabli na pocetku programa
    //ovde im se dodeljuje vrednost
    scores = [0, 0];
    activePlayer = 0;
    roundScore = 0;
    gamePlaying = true;

//selektovanja DOM-a na html-u
    document.querySelector('#dice-1').style.display = 'none';
    document.querySelector('#dice-2').style.display = 'none';
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');
}




