/*
 GAME RULES:
 
 - The game has 2 players, playing in rounds 
 //array var socres declaration
 - In each turn, a player rolls a dice as many times as he 
 whishes. 
 Each result get added to his ROUND score
 //var round starts from 0 declaration
 - BUT, if the player rolls a 1, 
 all his ROUND score gets lost. 
 After that, it's the next player's turn
 //if statement
 
 - The player can choose to 'Hold', 
 which means that his ROUND score gets 
 added to his GLBAL score. After that, it's the next 
 player's turn
 //if pa event listener 
 - The first player to reach 100 points on 
 GLOBAL score wins the game
 */
var scores, roundScore, activePlayer, gamePlaying;
//state variable tells us condition of the system , kada hocemo da iskazemo stanje necega
//da li se igra igra ili ne true ili false gamePlaying

//start game
init();
//dodavanje event listenera za roll kocke funkcionalnost
document.querySelector('.btn-roll').addEventListener('click', function () {
    if (gamePlaying) {
        //Random number
        var dice = Math.floor(Math.random() * 6) + 1;

        //Display the result
        var diceDOM = document.querySelector('.dice');
        diceDOM.style.display = "block";
        diceDOM.src = 'dice-' + dice + '.png';

        //update the round score if the rolled number was not a 1
        if (dice !== 1) {
            roundScore += dice;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
    }

});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        scores[activePlayer] += roundScore;

        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        if (scores[activePlayer] >= 20) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none!';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;
        } else {
            nextPlayer();
        }
    }

});


function nextPlayer() {
    //predji na drugog igraca
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    //podesi sve vrednosti na nulu
    roundScore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    //podesi da je aktivan drugi igrac
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    //ne vidi se kocka dok se ne klikne na roll button
    document.querySelector('.dice').style.display = 'none';
}
//ako se izabere nova igra pokrece se ponovo funkcija init
//klik na button new game
document.querySelector('.btn-new').addEventListener('click', init);
//init
function init() {
    //definisanje osnovnih globalnih varijabli na pocetku programa
    //ovde im se dodeljuje vrednost
    scores = [0, 0];
    activePlayer = 0;
    roundScore = 0;
    gamePlaying = true;

//selektovanja DOM-a na html-u
    document.querySelector('.dice').style.display = 'none';
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');
}

