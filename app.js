/*
 GAME RULES:
 
 - The game has 2 players, playing in rounds 
 //array var socres declaration
 - In each turn, a player rolls a dice as many times as he 
 whishes. 
 Each result get added to his ROUND score
 //var round starts from 0 declaration
 - BUT, if the player rolls a 1, 
 all his ROUND score gets lost. 
 After that, it's the next player's turn
 //if statement
 
 - The player can choose to 'Hold', 
 which means that his ROUND score gets 
 added to his GLBAL score. After that, it's the next 
 player's turn
 //if pa event listener 
 - The first player to reach 100 points on 
 GLOBAL score wins the game
 */

var scores, roundScore, activePlayer;
function init() {
    activePlayer = 0;
    scores = [0, 0];
//konacan zaokruzen rezultat igraca posle svakog bacanja
    roundScore = 0;
//var activePlayer prati koji igrac baca kocku
//using Math object random ali ga moras zaoukruziti na 
//ceo broj izmedju 1 i 6 sto obezbedjuje naredba floor()
//dice = 6; //6 je ovde default vrednost
//Math.random() * 6 znaci od 0 do 6 a floor je ceo broj
//+1 sluzi da ne dobijemo 0 na kocki
//dice = Math.floor(Math.random() * 6)+1;
//console.log(dice);
//sada cemo da manipulisemo sa DOM
//document.querySelector('#current-' + activePlayer).textContent = dice;
//document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '</em>';

    document.querySelector('.dice').style.display = 'none';

    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
}
init();

function btn() {}
//ovo ispod je primer call back funkcije kada jedna funkcija zove drugu
//document.querySelector('.btn-roll').addEventListener('click', btn());
//document.querySelector('.btn-roll').addEventListener('click', function(){'ja sam anonimus function'});

document.querySelector('.btn-roll').addEventListener('click', function () {
    //1. Random number on dice
    var dice = Math.floor(Math.random() * 6) + 1;
    //display result 
    var diceDOM = document.querySelector('.dice');
    diceDOM.style.display = "block";
    diceDOM.src = 'dice-' + dice + '.png'
    //update the round score if it is not 1
    if (dice !== 1) {
        //add score
        roundScore += dice;
        //ovo ispod je to isto samo drugi nacin
//        roundScore = roundScore + dice;
        document.querySelector('#current-' + activePlayer).textContent = roundScore;
//        console.log(roundScore);
    } else {
        //next player
        nextPlayer();
//        console.log(roundScore);
    }
    //provera koji brojevi se dobijaju zbog pracenje broja 1
    console.log(dice);
//HOLD BUTTON
    document.querySelector('.btn-hold').addEventListener('click', function () {
        //add current score toglobal score
        scores[activePlayer] = roundScore;
//Update the UI for total score
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
        console.log(activePlayer);
        //Check if player won the game
        if (scores[activePlayer] >= 100) {
            document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.toggle('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.toggle('active');
        } else {
            nextPlayer();
        }

    });
});
//posto se kod prebacivanja na sledeceg igraca kod dobijanja 1 i pritiskom
//na dugme hold desava ista radnja def cemo funkciju koju pozivamo u tim
//slucajevima
function nextPlayer() {
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
//    roundScore = 0;
//
//    document.getElementById('current-0').textContent = '0';
//    document.getElementById('current-1').textContent = '0';
//        document.querySelector('.player-0-panel').classList.remove('active');
//        document.querySelector('.player-1-panel').classList.add('active');
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');

//    document.querySelector('.dice').style.display = 'none';
}

document.querySelector('.btn-new').addEventListener('click', init());

